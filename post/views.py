from django.shortcuts import render, get_object_or_404
from post.models import Post


def post_list(request):
    # вывод всех постов
    posts = Post.objects.all()
    return render(request, "post/post_list.html", {"posts": posts})

def post_single(request, pk):
    # вывод одного поста
    # posts = Post.objects.get(id=pk)  -- вывести статью по номеру id. если статьи не будет, то выдасть ошибку
    post = get_object_or_404(Post, id=pk)
    return render(request, "post/post_single.html", {"post": post})