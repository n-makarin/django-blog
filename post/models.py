from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()



class Category(models.Model):
    #Класс категорий статей
    title = models.CharField("Название", max_length=50)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


    def __str__(self):
        return self.title


class Tag(models.Model):
    #Класс тегов статей
    title = models.CharField("Тег", max_length=50)

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"


    def __str__(self):
        return self.title


class Post(models.Model):
    #Класс постов
    user = models.ForeignKey( # значит, что у одного автора мб много таких новостей
        User,
        verbose_name="Автор", 
        on_delete=models.CASCADE) # удалили юзера - удалили все его статьи

    categoty = models.ForeignKey( # у одной категории мб много новостей
        Category, 
        verbose_name = "Категория",
        on_delete=models.SET_NULL, # удалили категорию - статьи остались
        null=True) # указали, что поле категории при удалении самой категории остаться мустым

    title = models.CharField("Заголовок", max_length=100)
    text_min = models.TextField("Мин. текст", max_length=350)
    text = models.TextField("Текст статьи")
    tag = models.ManyToManyField(Tag, verbose_name="Теги") # может быть выбор из нескольких тегов (футбол/хоккей)
    created = models.DateTimeField("Дата создания", auto_now_add=True)
    description = models.CharField("Описание", max_length=100)
    keywords = models.CharField('Ключевые слова', max_length=50)

    class Meta:
        verbose_name = "Статья"
        verbose_name_plural = "Статьи"


    def __str__(self):
        return self.title