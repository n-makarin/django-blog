from django.urls import path
from . import views

urlpatterns = [
        # вывод списка статей
    path('', views.post_list, name="posts_list"),
        # вывод полной статьи
    path('single/<int:pk>', views.post_single, name="post_single")
]